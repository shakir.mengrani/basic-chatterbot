from chatterbot.logic import LogicAdapter
import datetime
import requests

class MyLogicAdapter(LogicAdapter):
    get_word = None
    def __init__(self, **kwargs):
        super(MyLogicAdapter, self).__init__(**kwargs)

    def can_process(self, statement):
        words = ['weather', 'time', 'date']
        for x in words:
            try:
                if statement.text.split().index(x):
                    self.get_word = x
            except ValueError as e:
                pass
        return True if self.get_word is not None else False

    def process(self, statement):
        from chatterbot.conversation import Statement
        #add some logic
        # my_answer_list = {'music': ['Hello, party']}
        my_answer_list = {
            'weather': self.getWeather(), 'time': datetime.datetime.now().strftime("%I:%M:%S"), 'date': datetime.datetime.now().strftime("%d-%h-%Y")
        }
        try:
            response_statement = Statement(my_answer_list[self.get_word])
            response_statement.confidence = 1 # confidence should be 0 or 1
            return response_statement
        except:
            response_statement = Statement("No answer were found!")
            response_statement.confidence = 0 # confidence should be 0 or 1
            return response_statement

    def getWeather(self):
        return "yet not implemented !"