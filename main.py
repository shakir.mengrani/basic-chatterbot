from chatterbot import ChatBot
chatbot = ChatBot("Pagal",
    # trainer='chatterbot.trainers.ListTrainer',
    logic_adapters=[
        # 'chatterbot.logic.BestMatch',
       {'import_path': 'cool_adapter.MyLogicAdapter'}
    ],
)

# chatbot.train([
#     'What is a current date',
#     'What time is there',
#     'What weather is there',
# ])


def getResponse(statement):
    return  statement if statement.confidence else 'I didn\'t get you'

response = chatbot.get_response('i want to know about today\'s weather')
print(getResponse(response))

response = chatbot.get_response('What time is there')
print(getResponse(response))

response = chatbot.get_response('What date today')
print(getResponse(response))